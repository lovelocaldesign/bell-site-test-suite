<?php

require_once('vendor/autoload.php');
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\JavaScriptExecutor;

$timestamp = date("ymd_h.m.s");
$max_timeout = 120 * 1000; // longer timeout is needed for mobile simulators
$PROJECT = "BELL Annual Report";
$BUILD = $timestamp;

print $timestamp . "\n";

// array of capabilities used. OS versions are left blank, which means server randomly chooses.
$capslist = array(
	array("browser" => "IE", "browser_version" => "11.0", "resolution" => "1024x768", "os" => "windows"),
	array("browser" => "Edge", "browser_version" => "13.0", "resolution" => "1024x768", "os" => "windows"),
	array("browser" => "Chrome", "browser_version" => "54.0", "resolution" => "1920x1080", "os" => "Windows"),
	
	array("browser" => "Chrome", "browser_version" => "54.0", "resolution" => "1024x768", "os" => "OS X"),
	array("browser" => "Firefox", "browser_version" => "50.0", "resolution" => "1024x768", "os" => "OS X"),
	array("browser" => "Safari", "browser_version" => "10.0", "resolution" => "1024x768", "os" => "OS X"),

	/*array("browserName" => "iPhone", "platform" => "MAC"),
	array("browserName" => "iPad", "platform" => "MAC"),

	array("browserName" => "android", "platform" => "ANDROID", "device" => "Samsung Galaxy S5"),
	$caps = array("browserName" => "android", "platform" => "ANDROID", "device" => "HTC One M8")*/
	);

foreach ($capslist as &$caps) {
	try {

		// use project and build name
		$caps["build"] = $BUILD;
		$caps["project"] = $PROJECT;

		$web_driver = RemoteWebDriver::create("https://heatherlynnstryc1:jDcBjnwLaSXyk71EUGeg@hub-cloud.browserstack.com/wd/hub", $caps, $max_timeout, $max_timeout);

		$web_driver->get("http://lld.website/BELL");

		// create filename prefix based on capabilities used
		if(array_key_exists("browser", $caps)) {
			$capsid = $caps["os"] . "_" . $caps["browser"] . $caps["browser_version"];
		} else {
			$capsid = $caps["platform"] . "_" . $caps["browserName"];
			if(array_key_exists("device", $caps)) {
				$capsid .= $caps["device"];
			}
		}

		// display the currently running test
		print "running test: " . $capsid . "\n";

		// login
		$element = $web_driver->findElement(WebDriverBy::className("input"));
		if($element) {
			$element->sendKeys("password4BELL");
			$element->submit();
		}
		sleep(5);

		// screenshot, then go to highlights
		$web_driver->takeScreenshot("homepage_" . $capsid . ".png");
		$element = $web_driver->findElement(WebDriverBy::partialLinkText("2016 Highlights"));
		if($element) {
			$element->click();
		}

		// click on each of the section article buttons to trigger scrolling animation and content change
		sleep(3);
		$element = $web_driver->findElement(WebDriverBy::className("section1_article_title2"));
		$element->click();
		sleep(3);
		$element = $web_driver->findElement(WebDriverBy::className("section1_article_title3"));
		$element->click();
		sleep(3);
		$element = $web_driver->findElement(WebDriverBy::className("section1_article_title1"));
		$element->click();
		sleep(3);

		// take another screenshot
		$web_driver->takeScreenshot("highlights_" . $capsid . ".png");

		// click on the donor image
		$element = $web_driver->findElement(WebDriverBy::className("block-donor-image"));
		$element->click();
		sleep(2);

		$web_driver->quit();
	}
	catch(Exception $e) {
		// error handling: try to close the connection
		if(isset($web_driver)) {
			$web_driver->quit();
		}
		// print any errors that occur, but continue with testing
		echo 'ERROR: ' . $e->getMessage() . "\n";
	}
}

?>